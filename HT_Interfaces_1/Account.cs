﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HT_Interfaces_1
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

    }
}
