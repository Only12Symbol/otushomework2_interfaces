﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace HT_Interfaces_1
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream Data;
        private readonly ISerializer Serializer;

        public OtusStreamReader(Stream data, ISerializer serializer)
        {
            Data = data;
            Serializer = serializer;
        }
        public IEnumerator<T> GetEnumerator()
        {
            T[] desArr = Serializer.Deserialize<T[]>(Data);

            if (Data != null)
            {
                foreach(var element in desArr)
                {
                    yield return element;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            if (Data != null)
            {
                Data.Close();
            }
            GC.SuppressFinalize(this);
        }
    }
}
