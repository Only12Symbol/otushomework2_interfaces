﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HT_Interfaces_1
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> Repository;
        public AccountService(IRepository<Account> repository)
        {
            Repository = repository;
        }
        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account.FirstName))
            {
                throw new Exception("Firstname должно быть заполнено");
            }
            if (string.IsNullOrEmpty(account.LastName))
            {
                throw new Exception("Lastname должно быть заполнено");
            }

            DateTime today = DateTime.Today;
            TimeSpan age = today - account.BirthDate;
            double ageInYears = age.TotalDays / 365;

            if (ageInYears < 18)
            {
                throw new Exception("Возраст меньше 18 лет");
            }
            else
            {
                Repository.Add(account);
            }
        }
    }
}
