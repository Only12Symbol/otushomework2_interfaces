﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Threading;

namespace HT_Interfaces_1
{
   
    class Program
    {

        static void Main(string[] args)
        {
            Person[] Persons = new Person[]
            {
                new Person(15, "Руслан"),
                new Person(29, "Елена"),
                new Person("Валерий", 23),
                new Person("Александр", 54),
                new Person("Федор", 20)
            };

            Console.WriteLine("Транслируем список в объект XML:");

            ISerializer serializer = new OtusXmlSerializer();

            string dataXML = serializer.Serialize(Persons);

            File.WriteAllText("./persons", dataXML);
            Console.WriteLine(dataXML);
            Console.WriteLine();
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(dataXML));

            Console.WriteLine("Десериализуем XML:");
            OtusStreamReader<Person> StreamReader = new OtusStreamReader<Person>(stream, serializer);
            foreach (var ppl in StreamReader)
            {
                Console.WriteLine(ppl.ToString());
            }

            PersonSorter sorter = new PersonSorter();

            var sorted = sorter.Sort<Person>(Persons, "Age");
            Console.WriteLine();
            Console.WriteLine("Отсортированный список:");

            foreach (Person s in sorted)
            {
                Console.WriteLine(s.ToString());
            }

            Console.WriteLine();

            var repo = new AccountRepository("TestFile.txt");

            var accserv = new AccountService(repo);

            accserv.AddAccount(new Account()
            {
                FirstName = "Иван",
                LastName = "Петров",
                BirthDate = new DateTime(1996, 10, 09)
            });
            accserv.AddAccount(new Account()
            {
                FirstName = "Дима",
                LastName = "Сидоров",
                BirthDate = new DateTime(1992, 05, 03)
            });
            accserv.AddAccount(new Account()
            {
                FirstName = "Никита",
                LastName = "Жох",
                BirthDate = new DateTime(1976, 11, 09)
            });
            accserv.AddAccount(new Account()
            {
                FirstName = "Вася",
                LastName = "Синичкин",
                BirthDate = new DateTime(1991, 04, 04)
            });

            var text = repo.GetAll();
            foreach (var element in text)
            {
                Console.WriteLine("First name - {0}\n Last name - {1}\n Birth date - {2}", 
                    element.FirstName, 
                    element.LastName, 
                    element.BirthDate);
                Console.WriteLine();
            }

            Console.ReadKey();

        }
    }
}
