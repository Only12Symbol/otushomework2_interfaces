using NUnit.Framework;
using HT_Interfaces_1;
using Moq;
using ExtendedXmlSerializer;

namespace NUnitTestProject
{
    public class Tests
    {

        [Test]
        public void TestAddAccount()
        {
            var mock = new Mock<IRepository<Account>>();
            mock
                .Setup(rp => rp.Add(It.IsAny<Account>()))
                .Return(string.Empty);

            IAccountService accountService = new AccountService(mock.Object);
            accountService.AddAccount(new Account()
            {
                FirstName = "����",
                LastName = "������",
                BirthDate = new System.DateTime(1996, 10, 09)
            });
            accountService.AddAccount(new Account()
            {
                FirstName = "����",
                LastName = "", 
                BirthDate = new System.DateTime(2010, 10, 09)
                //TestAddAccount
                //������������: 424���

                //  ���������: 
                //    System.Exception : Lastname ������ ���� ���������
                //  ����������� �����: 
                //    AccountService.AddAccount(Account account) ������ 25
                //    Tests.TestAddAccount() ������ 26
            });

            mock.Verify(repository => repository.Add(It.IsAny<Account>()));
        }
    }
}